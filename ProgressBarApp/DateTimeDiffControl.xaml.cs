﻿using System;
using System.Windows.Controls;

namespace ProgressBarApp
{
    public partial class DateTimeDiffControl : UserControl
    {
        public DateTime DateTime{ get; set; }
        public TimeSpan DateTimeDiff { get; set; }

        public DateTimeDiffControl()
        {
            InitializeComponent();
            dateTimeLabel.DataContext = this;
            dateTimeDiffLabel.DataContext = this;
        }

    }
}
