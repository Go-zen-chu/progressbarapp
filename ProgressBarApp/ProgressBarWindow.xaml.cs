﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ProgressBarApp
{
    public partial class ProgressBarWindow : Window
    {
        string processName = "No Name";
        public string ProcessName
        {
            get { return processName;}
            set
            {
                this.processName = value;
                this.Title = "[ " + value + " ] Now In Progress ... ";
            }
        }

        public ProgressBar ProgressBar
        {
            get { return progressBar; }
        }

        public ProgressBarWindow()
        {
            InitializeComponent();
            progressItemsControl.Items.Add(new DateTimeDiffControl() { DateTime = DateTime.Now });
        }

        /// <summary>
        /// update progressbar and calculate difference between previous calculation
        /// </summary>
        /// <param name="oneTickValue"></param>
        public void UpdateInvoke(double oneTickValue = 1)
        {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                var prevDateTime = progressItemsControl.Items.Cast<DateTimeDiffControl>().Last().DateTime;
                var now = DateTime.Now;
                progressItemsControl.Items.Add(new DateTimeDiffControl() { DateTime = now, DateTimeDiff = now - prevDateTime });
                progressBar.Value += oneTickValue;
            }));
        }

        /// <summary>
        /// export process as csv
        /// </summary>
        /// <param name="exportPath"></param>
        public void ExportProgress(string exportPath)
        {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (progressItemsControl.Items == null || progressItemsControl.Items.Count == 0) return;
                using (var sw = new StreamWriter(exportPath))
                {
                    sw.WriteLine(processName);
                    foreach (var dtdc in progressItemsControl.Items.Cast<DateTimeDiffControl>())
                        sw.WriteLine(string.Join(",", dtdc.DateTime, dtdc.DateTimeDiff));
                }
            }));
        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            var sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.Filter = "*.csv|*.csv";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ExportProgress(sfd.FileName);
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void progressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (progressBar.Value == progressBar.Maximum)
            {
                exportButton.Visibility = System.Windows.Visibility.Visible;
                closeButton.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}
