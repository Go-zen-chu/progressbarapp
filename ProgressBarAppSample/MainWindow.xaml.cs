﻿using ProgressBarApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProgressBarAppSample
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var pw = new ProgressBarWindow();
            pw.ProcessName = "Sample Process";
            pw.ProgressBar.Minimum = 0;
            pw.ProgressBar.Maximum = 200;
            pw.Show();
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    statusLabel.Dispatcher.BeginInvoke(new Action(() => { statusLabel.Content = "status " + i; }));
                    System.Threading.Thread.Sleep(2000);
                    pw.UpdateInvoke(oneTickValue: 20);
                }
            });
        }
    }
}
